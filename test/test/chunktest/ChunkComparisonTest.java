package test.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Random;

import org.junit.Test;

import kivi.bigarrays.arrays.Chunk;
import kivi.bigarrays.arrays.ChunkType;
import kivi.bigarrays.arrays.PrimitiveChunkFactory;

public class ChunkComparisonTest{

	@Test
	public void comparesameChunks() {

		for (ChunkType type : ChunkType.values()) {

			Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);
			Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);

			assertEquals(a, b);

		}

	}

	@Test
	public void compareDifferentSizeChunks() {

		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 99, 100, 0, ChunkType.INT, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, ChunkType.INT, true);

		assertNotEquals(a, b);

		a = PrimitiveChunkFactory.getChunk(0, 0, 100, 99, 0, ChunkType.INT, true);
		b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, ChunkType.INT, true);

		assertNotEquals(a, b);

	}

	@Test
	public void compareDifferentTypeChunks() {

		for (ChunkType first : ChunkType.values()) {
			for (ChunkType second : ChunkType.values()) {
				if (first.equals(second))
					continue;
				Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, first, true);
				Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, second, true);

				assertNotEquals(a, b);
			}
		}

	}

	@Test
	public void compareSameContentChunks() {
		int hw = 100;
		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, hw, hw, 0, ChunkType.INT, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, hw, hw, 0, ChunkType.INT, true);

		Random r = new Random();
		for (int i = 0; i < 1000; i++) {
			int rand = r.nextInt(10000);
			int x = r.nextInt(hw);
			int y = r.nextInt(hw);
			a.setNumber(rand, x, y);
			b.setNumber(rand, x, y);
		}
		assertEquals(a, b);
	}

	@Test
	public void compareDifferentContentChunks() {
		int hw = 100;
		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, hw, hw, 0, ChunkType.INT, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, hw, hw, 0, ChunkType.INT, true);

		Random r = new Random();
		for (int i = 0; i < 1000; i++) {
			int rand = r.nextInt(10000);
			int x = r.nextInt(hw);
			int y = r.nextInt(hw);
			a.setNumber(rand, x, y);
			int x2 = r.nextInt(hw);
			int y2 = r.nextInt(hw);
			b.setNumber(rand, x2, y2);
		}
		assertNotEquals(a, b);
	}
}
