package test.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import kivi.bigarrays.arrays.Chunk;
import kivi.bigarrays.arrays.ChunkType;
import kivi.bigarrays.arrays.PrimitiveChunkFactory;

public class ChunkSerialization{

	@Test
	public void chunkSerializationDeserialisationTest() {
		for (ChunkType type : ChunkType.values()) {
			Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);
			Random r = new Random();
			for (int i = 0; i < 100000; i++) {
				a.setNumber(ThreadLocalRandom.current().nextLong() * Math.random(), r.nextInt(a.width),
						r.nextInt(a.height));
			}

			byte[] h = a.getBytes();

			Chunk b = Chunk.fromBytes(h);

			assertEquals(a, b);
		}
	}
}
