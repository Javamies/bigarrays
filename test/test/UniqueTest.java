package test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import kivi.bigarrays.arrays.BigArray;
import kivi.bigarrays.arrays.ChunkType;

public class UniqueTest{

	@Before
	public void setup() {
	}

	@Test
	public void uniqueRootViolationTest() {
		BigArray b = new BigArray(100, 100, new File("test"), 0, 0, 1, ChunkType.BYTE);

		assertThrows(IllegalArgumentException.class,
				() -> new BigArray(100, 100, new File("test"), 0, 0, 1, ChunkType.BYTE));

	}

	@Test
	@Ignore // TODO: what is the test idea. just doing the same root?
	public void sameRootTest() {
		BigArray a = new BigArray(100, 100, new File("test2"), 0, 0, 1, ChunkType.BYTE);

		BigArray b = new BigArray(100, 100, new File("test2"), 0, 0, 1, ChunkType.BYTE);

		assert a != null;
		assert b != null;
	}
}
