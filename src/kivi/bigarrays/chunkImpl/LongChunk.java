package kivi.bigarrays.chunkImpl;

import kivi.bigarrays.arrays.Chunk;
import kivi.bigarrays.arrays.ChunkType;

public class LongChunk extends Chunk {

	private long[][] data;

	public LongChunk(int i, int j, int width, int height, long defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.LONG);
		data = new long[width][height];
	}

	@Override
	public Number getNumber(int x, int y) {
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y) {
		data[x][y] = n.longValue();

//		System.out.println("Value:" + data[x][y]);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray() {
		return (T) data;
	}

	@Override
	public void init() {
		long l = defaultValue.longValue();
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				data[x][y] = l;
			}
		}
	}
}
