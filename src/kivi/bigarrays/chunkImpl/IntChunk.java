package kivi.bigarrays.chunkImpl;

import kivi.bigarrays.arrays.Chunk;
import kivi.bigarrays.arrays.ChunkType;

public class IntChunk extends Chunk {

	private int[][] data;

	public IntChunk(int i, int j, int width, int height, Number defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.INT);
		data = new int[width][height];
	}

	@Override
	public Number getNumber(int x, int y){
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y){
		data[x][y] = n.intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray(){
		return (T) data;
	}

	@Override
	public void init(){
		int b = this.defaultValue.intValue();
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				data[x][y] = b;
			}
		}
	}
}
