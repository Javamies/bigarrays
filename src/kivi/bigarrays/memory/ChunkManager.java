package kivi.bigarrays.memory;

import kivi.bigarrays.arrays.BigArray;

public abstract class ChunkManager{

	public abstract void release(BigArray big);

	public abstract boolean canCreate(int currentCount, int chunkSizeBytes);
}
