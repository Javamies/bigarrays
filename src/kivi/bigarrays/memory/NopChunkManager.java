package kivi.bigarrays.memory;

import kivi.bigarrays.arrays.BigArray;

public class NopChunkManager extends ChunkManager{

	@Override
	public void release(BigArray big) {

	}

	@Override
	public boolean canCreate(int currentCount, int chunkSizeBytes) {
		return true;
	}

}
