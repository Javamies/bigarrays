package kivi.bigarrays.utils;

public class Point{

	public final int x;
	public final int y;

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Point) {
			Point k = (Point) obj;
			return k.x == this.x && k.y == this.y;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "("+x+","+y+")";
	}

}
