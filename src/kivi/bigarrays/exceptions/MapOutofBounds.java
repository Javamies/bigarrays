package kivi.bigarrays.exceptions;

import kivi.bigarrays.arrays.BigArray;

public class MapOutofBounds extends RuntimeException{

	private BigArray context;

	public void setContext(BigArray context) {
		this.context = context;
	}

	public BigArray getContext() {
		return context;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
