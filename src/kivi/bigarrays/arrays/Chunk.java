package kivi.bigarrays.arrays;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// ---------------- CHUNK CLASS-----------------------//

/**
 * Represents a single array in the (in)finite grid. Has references to adjacent
 * chunks for fast access.
 * 
 * @author Vilho
 *
 */
public abstract class Chunk{
	public static final int HEADER_LENGHT = 1 + 1 + 4 + 4 + 4 + 4 + 8;
	public final Number defaultValue;
	protected Number zero;
	protected final ChunkType type;
	/*
	 * size of this chunk. used for calculating if certain value is within bounds.
	 */
	public final int width;
	public final int height;

	/*
	 * Neighboring chunks
	 */
	protected Chunk up;
	protected Chunk down;
	protected Chunk left;
	protected Chunk right;

	/*
	 * Assuming 0,0 is origo and positive values expand up and right on a
	 * traditional coordinates
	 */
	public final int lowerLeftX;
	public final int lowerLeftY;

	/*
	 * The iteration when this chunk was accessed previously
	 */
	private long lastUsed;

	public Chunk(int i, int j, int width, int height, Number defaultValue, ChunkType c) {
		this.type = c;
		lowerLeftX = i;
		lowerLeftY = j;
		this.width = width;
		this.height = height;
		this.defaultValue = defaultValue;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Chunk getUp() {
		return up;
	}

	public Chunk getDown() {
		return down;
	}

	public Chunk getLeft() {
		return left;
	}

	public Chunk getRight() {
		return right;
	}

	public void setUp(Chunk up) {
		this.up = up;
	}

	public void setDown(Chunk down) {
		this.down = down;
	}

	public void setLeft(Chunk left) {
		this.left = left;
	}

	public void setRight(Chunk right) {
		this.right = right;
	}

	/**
	 * Removes the chunks connections from adjacent chunks
	 */
	void disconnect() {
		if (left != null) {
			left.right = null;
		}
		if (right != null) {
			right.left = null;
		}
		if (up != null) {
			up.down = null;
		}
		if (down != null) {
			down.up = null;
		}
	}

	/**
	 * returns wether or not this chunk begins from these coordinates.
	 * 
	 * @param xx
	 * @param yy
	 * @return
	 */
	public boolean matches(int xx, int yy) {
		return this.lowerLeftX == xx && this.lowerLeftY == yy;
	}

	/**
	 * returns whether or not a certain coordinate is within the bounds of this
	 * chunk.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean contains(int x, int y) {
		if (x >= lowerLeftX && x < lowerLeftX + width && y >= lowerLeftY && y < lowerLeftY + height) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[" + lowerLeftX + "," + lowerLeftY + "]";
	}

	public long getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(long lastUsed) {
		this.lastUsed = lastUsed;
	}

	public boolean isEmpty() {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (getNumber(x, y).longValue()!= defaultValue.longValue()) {
					return false;
				}
			}
		}
		System.out.println("empty!");
		return true;
	}

	public final ChunkType getChunkType() {
		return type;
	}

	/**
	 * Get this chunks byte representation. First 4 bytes are the edge length of the
	 * chunks. Next 8 are the x coordinate and then y coordinate. Finally all the
	 * data rows.
	 * 
	 * @return
	 */
	public byte[] getBytes() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(version);
		baos.write(type.type); // chunkType magic number

		int[] values = { width, height, lowerLeftX, lowerLeftY };

		// write rest of the header
		for (int k : values) {
			try {
				baos.write(ByteBuffer.allocate(4).putInt(k).array());
			} catch (IOException e) {
				Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
			}
		}

		try {
			// default value as 8 bytes
			baos.write(numberAsBytes(new Long(defaultValue.longValue())));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// write all the numbers
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				try {
					baos.write(numberAsBytes(getNumber(x, y)));

				} catch (IOException e) {
					Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
				}
			}
		}
		return baos.toByteArray();
	}

	public static Chunk fromBytes(byte[] bytes) {
		ChunkType k = ChunkType.valueOf(bytes[1]).get();
//		System.out.println("lChunkType is " +k);
		int[] values = new int[4];
		byte[] array = new byte[4];
		int start = 2;
		for (int i = 0; i < 4; i++) {
			System.arraycopy(bytes, start, array, 0, 4);
			values[i] = ByteBuffer.wrap(array).order(ByteOrder.BIG_ENDIAN).getInt();
			start += 4;

		}

		byte[] defaultval = new byte[8];
		System.arraycopy(bytes, start, defaultval, 0, 8);
		start += 8;

		int width = values[0];
		int height = values[1];
		int lowerleftx = values[2];
		int lowerlefty = values[3];

		Chunk c = PrimitiveChunkFactory.getChunk(lowerleftx, lowerlefty, width, height,
				bytesAsNumber(defaultval, ChunkType.LONG), k, false);

		switch (k) {
			case BYTE:
				int i = start;
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						c.setNumber(bytes[i], x, y);
						i++;
					}
				}
				break;
			case SHORT:
			case INT:
			case LONG:
			case DOUBLE:
			case FLOAT:
				int size = k.bytesPerValue;

				byte[] b = new byte[size];

				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						System.arraycopy(bytes, start, b, 0, size);
						c.setNumber(bytesAsNumber(b, k), x, y);
						start += size;
					}
				}
		}
		return c;
	}

	public abstract Number getNumber(int x, int y);

	public abstract void setNumber(Number n, int x, int y);

	public abstract <T> T getArray();

	public abstract void init();

	static int version = 0;

	private byte[] numberAsBytes(Number k) {

		if (k instanceof Byte) {
			return new byte[] { k.byteValue() };
		} else if (k instanceof Short) {
			return ByteBuffer.allocate(2).putShort(k.shortValue()).array();
		} else if (k instanceof Integer) {
			return ByteBuffer.allocate(4).putInt(k.intValue()).array();
		} else if (k instanceof Long) {
			return ByteBuffer.allocate(8).putLong(k.longValue()).array();
		} else if (k instanceof Float) {
			return ByteBuffer.allocate(4).putFloat(k.floatValue()).array();
		} else if (k instanceof Double) {
			return ByteBuffer.allocate(8).putDouble(k.doubleValue()).array();
		}
		return new byte[0];
	}

	private static Number bytesAsNumber(byte[] b, ChunkType c) {
		ByteBuffer bb = ByteBuffer.wrap(b);
		switch (c) {
			case BYTE:
				return b[0];
			case SHORT:
				return bb.getShort();
			case INT:
				return bb.getInt();
			case LONG:
				return bb.getLong();
			case FLOAT:
				return bb.getFloat();
			case DOUBLE:
				return bb.getDouble();
			default:
				break;
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o instanceof Chunk) {
			Chunk c = (Chunk) o;

			if (c.height != this.height || c.width != this.width)
				return false;
			if (c.type != this.type)
				return false;
			if (!defaultValue.equals(c.defaultValue))
				return false;
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (!this.getNumber(x, y).equals(c.getNumber(x, y))) {
						return false;
					}
				}

			}
			return true;
		}
		return false;

	}

//	public boolean contentEquals(Chunk other) {
//		if (this.height == other.height && this.width == other.width) {
//			for (int x = 0; x < width; x++) {
//				for (int y = 0; y < height; y++) {
//					double a = this.getNumber(x, y).doubleValue();
//					double b = this.getNumber(x, y).doubleValue();
//					
//				}
//			}
//		}
//		return false;
//	}
}
