package kivi.bigarrays.arrays;

import java.util.Arrays;
import java.util.Optional;

public enum ChunkType {
	BYTE(0, 1), SHORT(1, Short.BYTES), INT(2, Integer.BYTES), LONG(3, Long.BYTES), FLOAT(5, Float.BYTES), DOUBLE(6, Double.BYTES);

	public final byte type;
	public final int bytesPerValue;

	private ChunkType(int i, int bytes){
		type = (byte) i;
		this.bytesPerValue = bytes;
	}

	public static Optional<ChunkType> valueOf(int type){
		return Arrays.stream(values()).filter(typeNumber -> typeNumber.type == type).findFirst();
	}
}
