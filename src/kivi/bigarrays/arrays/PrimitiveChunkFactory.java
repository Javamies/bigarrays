package kivi.bigarrays.arrays;

import java.util.function.BiFunction;

import kivi.bigarrays.chunkImpl.ByteChunk;
import kivi.bigarrays.chunkImpl.DoubleChunk;
import kivi.bigarrays.chunkImpl.FloatChunk;
import kivi.bigarrays.chunkImpl.IntChunk;
import kivi.bigarrays.chunkImpl.LongChunk;
import kivi.bigarrays.chunkImpl.ShortChunk;

public class PrimitiveChunkFactory{

	private BiFunction<Integer, Integer, Chunk> newChunk;
	private int bytes;

	public PrimitiveChunkFactory(int width, int height, Number basevalue, ChunkType primitive) {
		switch (primitive) {
			case BYTE:
				newChunk = (x, y) -> new ByteChunk(x, y, width, height, basevalue.byteValue());
				break;
			case SHORT:
				newChunk = (x, y) -> new ShortChunk(x, y, width, height, basevalue.shortValue());
				break;
			case INT:
				newChunk = (x, y) -> new IntChunk(x, y, width, height, basevalue.intValue());
				break;
			case LONG:
				newChunk = (x, y) -> new LongChunk(x, y, width, height, basevalue.longValue());
				break;
			case FLOAT:
				newChunk = (x, y) -> new FloatChunk(x, y, width, height, basevalue.floatValue());
				break;
			case DOUBLE:
				newChunk = (x, y) -> new DoubleChunk(x, y, width, height, basevalue.doubleValue());
				break;
			default:
				break;

		}
		bytes = width * height * primitive.bytesPerValue;

	}

	public int chunkSizeBytes() {
		return bytes;
	}

	public Chunk get(int x, int y) {
		return newChunk.apply(x, y);
	}

	public Chunk getAndInit(int x, int y) {
		Chunk c = newChunk.apply(x, y);
		c.init();
		return c;
	}

	public static Chunk getChunk(int i, int j, int width, int height, Number basevalue, ChunkType primitive,
			boolean init) {
		Chunk k = new PrimitiveChunkFactory(width, height, basevalue, primitive).get(i, j);

		if (init) {
			k.init();
		}
		return k;
	}

}
