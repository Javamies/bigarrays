package kivi.bigarrays.arrays;

import java.io.File;
import java.io.IOException;

public class BBABuilder{

	protected int w = 100;
	protected int h = 100;
	protected String id = "default_ID_" + System.nanoTime();
	protected File root = new File(id);
	protected long maxChunksOnDisk = -1;
	protected int maxChunksLoaded = -1;
	private Number defaultvalue = 0;
	private ChunkType c = ChunkType.INT;

	public BBABuilder() {

	}

	public BBABuilder setWidth(int w) {
		this.w = w;
		return this;
	}

	public BBABuilder setHeight(int h) {
		this.h = h;
		return this;
	}

	public BBABuilder setId(String id) {
		this.id = id;
		return this;
	}

	public BBABuilder setRoot(File root) {
		this.root = root;
		return this;
	}

	public BBABuilder setDefaultValue(Number b) {
		this.defaultvalue = b;
		return this;
	}

	public BBABuilder setMaxChunksOnDisk(int maxChunksOnDisk) {
		this.maxChunksOnDisk = maxChunksOnDisk;
		return this;
	}

	public BBABuilder setChunkType(ChunkType c) {
		this.c = c;
		return this;
	}

	public BBABuilder setMaxChunksLoaded(int maxChunksLoaded) {
		this.maxChunksLoaded = maxChunksLoaded;
		return this;
	}

	public BigArray build() throws IOException {
		return new BigArray(w, h, root, maxChunksOnDisk, maxChunksLoaded, defaultvalue, c);
	}
}
